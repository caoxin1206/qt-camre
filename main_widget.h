#ifndef MAIN_WIDGET_H
#define MAIN_WIDGET_H

#include <QWidget>
#include "QToolButton"
#include "QLabel"
#include "QTimer"
#include "QString"
#include "QPixmap"
#include "QPalette"
#include "QMatrix"
#include "QImage"
#include "QBrush"
#include "QFileDialog"
#include "QMessageBox"
//#include "QDebug"


namespace Ui {
class main_Widget;
}

class main_Widget : public QWidget
{
    Q_OBJECT

public:
    explicit main_Widget(QWidget *parent = 0);
    ~main_Widget();



private slots:
    void on_open_BTN_clicked();

    void on_next_BTN_clicked();

    void on_previous_BTN_clicked();

    void on_close_BTN_clicked();

private:
    Ui::main_Widget *ui;

    QPixmap pix;           //显示图片
    int i;
    int j;
    int mm;
    QString imageList[100];    //图片名称字符串list
    //qreal是Qt的数据类型，在桌面操作系统中（比如Windows， XNix等）qreal其实就是double类型；
    //而在嵌入设备系统中，qreal则等同于float 类型。
    qreal w,h;
    QString image_sum,image_position;
    QStringList::Iterator it;

};

#endif // MAIN_WIDGET_H
