#include "main.h"
#include <QApplication>
#include "v4l2.h"

main_Widget *main_WD;
menu_widget *menu_WD;
get_photo_widget *get_photo_WD;
V4L2 v4l2;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    v4l2.V4l2_Init();
    menu_WD = new menu_widget;
    menu_WD->show();

    return a.exec();
}
