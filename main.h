#ifndef MAIN_H
#define MAIN_H
#include <QDesktopWidget>
#include <QStyle>
#include <QRect>
#include <QDir>
#include <QDebug>
#include "main_widget.h"
#include "menu_widget.h"
#include "get_photo_widget.h"

extern V4L2 v4l2;
extern main_Widget *main_WD;
extern menu_widget *menu_WD;
extern get_photo_widget *get_photo_WD;

#endif // MAIN_H

