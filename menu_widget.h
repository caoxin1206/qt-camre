#ifndef MENU_WIDGET_H
#define MENU_WIDGET_H

#include <QWidget>

namespace Ui {
class menu_widget;
}

extern "C"{
char *getip();
}

class menu_widget : public QWidget
{
    Q_OBJECT

public:
    explicit menu_widget(QWidget *parent = 0);
    ~menu_widget();

private slots:
    void on_photos_widget_BTN_clicked();

    void on_take_photo_widget_BTN_clicked();

private:
    Ui::menu_widget *ui;
};

#endif // MENU_WIDGET_H
