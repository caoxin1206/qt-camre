#include "main.h"
#include "menu_widget.h"
#include "ui_menu_widget.h"


#include <stdarg.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <ifaddrs.h>

menu_widget::menu_widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::menu_widget)
{
    ui->setupUi(this);
    QDesktopWidget *deskTopWidget = QApplication::desktop();
    QRect deskRect = deskTopWidget->availableGeometry();

    int appH = deskRect.height();
    int appW = deskRect.width();
    this->setFixedSize(appW, appH);
    setGeometry(0, 0, appW, appH);

    QString my_ip= "本地IP:";
    my_ip += getip();
    qDebug()<<my_ip;
    ui->ip_label->setText(my_ip);
}


char *getip()
{
    struct ifaddrs *ifaddr, *ifa;
    int family, s;
    char *host = NULL;

    if (getifaddrs(&ifaddr) == -1) {
        perror("getifaddrs");
        return NULL;
    }

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == NULL)
            continue;

        family = ifa->ifa_addr->sa_family;

        if (!strcmp(ifa->ifa_name, "lo"))
            continue;
        if (family == AF_INET) {
            if ((host = (char*)malloc(NI_MAXHOST)) == NULL)
                return NULL;
            s = getnameinfo(ifa->ifa_addr,
                    (family == AF_INET) ? sizeof(struct sockaddr_in) :
                    sizeof(struct sockaddr_in6),
                    host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
            if (s != 0) {
                return NULL;
            }
            freeifaddrs(ifaddr);
            return host;
        }
    }
    return NULL;
}

menu_widget::~menu_widget()
{
    delete ui;
}

void menu_widget::on_photos_widget_BTN_clicked()
{
    main_WD = new main_Widget;
    main_WD->show();

    menu_WD->close();
    delete menu_WD;
}

void menu_widget::on_take_photo_widget_BTN_clicked()
{
    get_photo_WD = new get_photo_widget;
    get_photo_WD->show();

    menu_WD->close();
    delete menu_WD;
}
