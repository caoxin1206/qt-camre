#ifndef GET_PHOTO_WIDGET_H
#define GET_PHOTO_WIDGET_H

#include <QWidget>
#include "v4l2.h"
#include "QToolButton"
#include "QLabel"
#include "QTimer"
#include "QString"
#include "QPixmap"
#include "QPalette"
#include "QMatrix"
#include "QImage"
#include "QBrush"
#include "QFileDialog"
#include "QMessageBox"

namespace Ui {
class get_photo_widget;
}

class get_photo_widget : public QWidget
{
    Q_OBJECT

public:
    explicit get_photo_widget(QWidget *parent = 0);
    ~get_photo_widget();

    void display_info(const QString buf);

    void display_picture(const QString filename);

private slots:
    void on_get_photo_BTN_clicked();

    void on_close_BTN_clicked();

private:
    Ui::get_photo_widget *ui;
};

#endif // GET_PHOTO_WIDGET_H
