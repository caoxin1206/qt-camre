#include "main.h"
#include "get_photo_widget.h"
#include "ui_get_photo_widget.h"
#include "v4l2.h"

get_photo_widget::get_photo_widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::get_photo_widget)
{
    ui->setupUi(this);
    QDesktopWidget *deskTopWidget = QApplication::desktop();
    QRect deskRect = deskTopWidget->availableGeometry();

    int appH = deskRect.height();
    int appW = deskRect.width();
    this->setFixedSize(appW, appH);
    setGeometry(0, 0, appW, appH);



}

get_photo_widget::~get_photo_widget()
{

    delete ui;
}

void get_photo_widget::display_info(const QString buf)
{
    ui->info_label->setText(buf);
}

void get_photo_widget::display_picture(const QString filename)
{
    QPixmap pix;           //显示图片
    pix.load(filename);
    pix = pix.scaled(ui->picture_label->width(), ui->picture_label->height(), Qt::KeepAspectRatio);
    ui->picture_label->setPixmap(pix);
}

void get_photo_widget::on_get_photo_BTN_clicked()
{
    ui->get_photo_BTN->setDisabled(true);
    display_info(QStringLiteral("图像采集中..."));
    v4l2.V4l2_Malloc();
    v4l2.V4l2_capturing();
    v4l2.Take_photo();
    v4l2.V4l2_Close();
    ui->get_photo_BTN->setEnabled(true);


}

void get_photo_widget::on_close_BTN_clicked()
{
    menu_WD = new menu_widget;
    menu_WD->show();

    get_photo_WD->close();
    delete get_photo_WD;
}
