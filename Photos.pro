#-------------------------------------------------
#
# Project created by QtCreator 2022-04-24T08:45:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Photos
TEMPLATE = app


SOURCES += main.cpp\
        main_widget.cpp \
        v412.cpp \
        menu_widget.cpp \
        get_photo_widget.cpp \
    yuv_to_jpg.cpp

HEADERS  += main_widget.h \
        menu_widget.h \
        main.h \
        v4l2.h \
        get_photo_widget.h \
    yuv_to_jpg.h

FORMS    += main_widget.ui \
        menu_widget.ui \
        get_photo_widget.ui

LIBS += -L /home/qq1253176313/ubuntu_share/samba/software/jpeg-8b/_install_dir/lib -ljpeg
INCLUDEPATH += /home/qq1253176313/ubuntu_share/samba/software/jpeg-8b/_install_dir/include/
