#include "main.h"
#include "main_widget.h"
#include "ui_main_widget.h"


//const QString picture_dir = "/home/qq1253176313/ubuntu_share/samba/picture";
const QString picture_dir = "/picture/";

main_Widget::main_Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::main_Widget)
{
    ui->setupUi(this);
    QDesktopWidget *deskTopWidget = QApplication::desktop();
    QRect deskRect = deskTopWidget->availableGeometry();

    int appH = deskRect.height();
    int appW = deskRect.width();
    this->setFixedSize(appW, appH);
    setGeometry(0, 0, appW, appH);

    ui->next_BTN->setDisabled(true);
    ui->previous_BTN->setDisabled(true);

}

main_Widget::~main_Widget()
{
    delete ui;
}

void main_Widget::on_open_BTN_clicked()
{
    QFileDialog FileDialog;
    ui->next_BTN->setDisabled(true);
    ui->previous_BTN->setDisabled(true);

    FileDialog.setFixedSize(this->width(),this->height());
    FileDialog.setGeometry(0,0,this->width(),this->height());
    FileDialog.setViewMode(QFileDialog::List);
//    FileDialog.setOption(QFileDialog::ShowDirsOnly);
    FileDialog.setDirectory(picture_dir);
    FileDialog.setFileMode(QFileDialog::ExistingFile);
    FileDialog.exec();
    QDir dir = FileDialog.directory();
    if(!dir.exists()){
        QMessageBox::critical(this,tr("错误"),tr("文件夹找不到"));
        return;
    }

    QStringList selectedFile = FileDialog.selectedFiles();  //get selected file
    if(selectedFile.isEmpty()){
        qDebug()<<"not selectedFile";
        return;
    }
    qDebug()<<"selectedFile.at(0):"<<selectedFile.at(0);


    QStringList filters;
    filters << "*.JPG" << "*.JPEG" << "*.PNG" << "*.BMP" \
               "*.jpg" << "*.jpeg" << "*.png" << "*.bmp";
    dir.setNameFilters(filters);





    QFileInfoList picture_names = dir.entryInfoList();
    picture_names.count();
    int k = 0;
    i = 1;
    foreach(QFileInfo picture_name, picture_names)
    {
        k++;
        qDebug()<<picture_name.absoluteFilePath();
        imageList[k] = picture_name.absoluteFilePath();
        if(selectedFile.at(0) == imageList[k])
            i=k;
    }

    if(k>0){
        ui->next_BTN->setEnabled(true);
        ui->previous_BTN->setEnabled(true);
    }

    j = k;


    image_sum = QString::number(k);
    image_position = QString::number(i);
    ui->picture_num_label->setText(tr("%1 / %2").arg(image_position).arg(image_sum));

    ui->picture_dir_label->setText(imageList[i]);

    pix.load(imageList[i]);
    w = ui->picture_label->width();
    h = ui->picture_label->height();
    pix = pix.scaled(w,h,Qt::KeepAspectRatio);
    ui->picture_label->setPixmap(pix);
}

void main_Widget::on_next_BTN_clicked()
{
    i++;
    if(i > j) i = 1;

    image_position = QString::number(i);
    ui->picture_num_label->setText(tr("%1 / %2").arg(image_position).arg(image_sum));

    ui->picture_dir_label->setText(imageList[i]);
    pix.load(imageList[i]);
    w = ui->picture_label->width();
    h = ui->picture_label->height();
    pix = pix.scaled(w,h,Qt::KeepAspectRatio);
    ui->picture_label->setPixmap(pix);
}

void main_Widget::on_previous_BTN_clicked()
{
    i--;
    if(i < 1) i = j;

    image_position = QString::number(i);
    ui->picture_num_label->setText(tr("%1 / %2").arg(image_position).arg(image_sum));

    ui->picture_dir_label->setText(imageList[i]);
    pix.load(imageList[i]);
    w = ui->picture_label->width();
    h = ui->picture_label->height();
    pix = pix.scaled(w,h,Qt::KeepAspectRatio);
    ui->picture_label->setPixmap(pix);

}

void main_Widget::on_close_BTN_clicked()
{
    menu_WD = new menu_widget;
    menu_WD->show();

    main_WD->close();
    delete main_WD;
}
